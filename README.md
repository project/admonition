yADMONITION
==========

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Known issues
 * Maintainers

INTRODUCTION
------------

The Admonition module helps authors add and edit admonitions in their content.
An admonition is advice to a reader. The following admonition types
are currently supported with plans to add many more:

* Info
* Warning

For a full description of the module, visit the project page:
   https://drupal.org/project/admonition

To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/admonition


REQUIREMENTS
------------

This module requires a standard installation of Drupal 8.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

Go to the list of text formats at /admin/config/content/formats. Choose a
text format that you want to add admonitions support to (e.g., Full HTML).

Drag the admonition icon from the Available buttons onto the Active toolbar.

Add the following attributes to the "Allow HTML tags" list (adapt as needed for your installation):

```
<div class="admonition admonition--* ">
```

MAINTAINERS
-----------

Current maintainers:
* Bill Seremets (bserem) - https://drupal.org/u/bserem

Past maintainers:
* Kieran Mathieson (mathieso) - https://drupal.org/user/1028
