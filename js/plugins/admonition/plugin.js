/**
 * @file
 * Admonition CKEditor plugin.
 *
 * Basic plugin inserting admonition elements into the CKEditor editing area.
 */

(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('admonition', {

    // Register the icons.
    icons: 'admonition',

    // The plugin initialization logic goes inside this method.
    init: function (editor) {

      // Define an editor command that opens our dialog window.
      editor.addCommand('admonition', new CKEDITOR.dialogCommand('admonitionDialog'));

      // Create a toolbar button that executes the above command.
      editor.ui.addButton('admonition', {

        // The text part of the button (if available) and the tooltip.
        label: Drupal.t('Insert admonition'),

        // The command to execute on click.
        command: 'admonition',

        // The button placement in the toolbar (toolbar group name).
        toolbar: 'insert'

      });

      // Add CSS in the editor
      var pluginDirectory = this.path;
      editor.addContentsCss ( pluginDirectory + 'css/admonition.css' );

      // Register our dialog file, this.path is the plugin folder path.
      CKEDITOR.dialog.add('admonitionDialog', this.path + 'dialogs/admonition.js');
    }
  });

} (Drupal));
