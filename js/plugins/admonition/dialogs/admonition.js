/**
 * @file
 * Defines dialog for Admonition CKEditor plugin.
 */

(function (Drupal) {

  'use strict';

  // Dialog definition.
  CKEDITOR.dialog.add('admonitionDialog', function (editor) {

    return {

      // Basic properties of the dialog window: title, minimum size.
      title: Drupal.t('Admonition properties'),
      minWidth: 400,
      minHeight: 150,

      // Dialog window content definition.
      contents: [
        {
          // Definition of the settings dialog tab.
          id: 'tab-settings',
          label: 'Settings',

          // The tab content.
          elements: [
            {
              // Text input field for the abbreviation text.
              type: 'select',
              id: 'type',
              label: Drupal.t('Type'),
              items: [
                [Drupal.t('Info'), 'info'],
                [Drupal.t('Warning'), 'warning']
              ],
              'default': 'info',

              // Validation checking whether the field is not empty.
              // Not really needed with the 'default' setting above.
              validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t('Type field cannot be empty.'))
            },
            {
              // Text input field for the admonition. Can be left emtpy and filled in the editor.
              type: 'text',
              id: 'text',
              label: Drupal.t('Text'),
            },
            {
              //Explain what an admonition is.
              type: 'html',
              html: '<label>' + Drupal.t('Legend') + ':</label>' +
              '<div class="admonition admonition--info">Info</div><br>' +
              '<div class="admonition admonition--warning">Warning</div>'
            },
          ]
        }
      ],

      // This method is invoked once a user clicks the OK button, confirming the
      // dialog.
      onOk: function () {

        // The context of this function is the dialog object itself.
        // See http://docs.ckeditor.com/#!/api/CKEDITOR.dialog.
        var dialog = this;

        // Create a new <abbr> element.
        var admonition = editor.document.createElement('div');

        // Set element attribute and text by getting the defined field values.
        admonition.setAttribute('class', 'admonition admonition--' + dialog.getValueOf('tab-settings', 'type'));
        admonition.setText(dialog.getValueOf('tab-settings', 'text'));

        // Finally, insert the element into the editor at the caret position.
        editor.insertElement(admonition);
      }
    };

  });

} (Drupal));
