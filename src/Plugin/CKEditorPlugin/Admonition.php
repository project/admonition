<?php

namespace Drupal\admonition\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Admonition" plugin.
 *
 * @CKEditorPlugin(
 *   id = "admonition",
 *   label = @Translation("Admonition"),
 *   module = "admonition"
 * )
 */
class Admonition extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = drupal_get_path('module', 'admonition');
    return [
      'admonition' => [
        'label' => $this->t('Admonition'),
        'image' => $module_path . '/js/plugins/admonition/icons/admonition.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'admonition') . '/js/plugins/admonition/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }
}
